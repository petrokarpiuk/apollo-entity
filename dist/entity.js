'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getOne = getOne;
exports.getAll = getAll;
exports.create = create;
exports.update = update;
exports.remove = remove;
function getOne(req, res, Entity) {
  var metadata = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var hooks = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  Entity.findById(req.params.id, function (err, entity) {
    if (err) return res.status(500).send('There was a problem finding ' + metadata.entityName + ': ' + err.message);

    if (!entity) return res.status(404).send(metadata.entityName + ' not found');

    if (hooks.transform) entities = hooks.transform(entities);
    if (hooks.callback) return hooks.callback(entities);

    return res.json(entity);
  });
}

function getAll(req, res, Entity) {
  var metadata = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var hooks = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  Entity.find({}, function (err, entities) {
    if (err) return res.status(500).send('There was a problem: ' + err.message);

    if (hooks.transform) entities = hooks.transform(entities);
    if (hooks.callback) return hooks.callback(entities);

    return res.json(entities);
  });
}

function create(req, res, Entity) {
  var metadata = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var hooks = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  var resource = metadata.fillable ? {} : req.body;

  if (metadata.fillable) {
    metadata.fillable.forEach(function (field) {
      resource[field] = req.body[field];
    });
  }

  if (hooks.transform) resource = hooks.transform(resource);

  var newEntity = new Entity(resource);

  newEntity.save(function (err, entity) {
    if (err) return res.status(500).send('There was a problem creating ' + metadata.entityName + ': ' + err.message);

    if (hooks.callback) return hooks.callback(entity);

    return res.json({ message: 'created', entity: entity });
  });
}

function update(req, res, Entity) {
  var metadata = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var hooks = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  Entity.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, entity) {
    if (err) return res.status(500).send('There was a problem updating a ' + metadata.entityName + ': ' + err.message);

    if (hooks.callback) return hooks.callback(entity);
    if (hooks.transform) entity = hooks.transform(entity);

    return res.json({ message: 'updated', entity: entity });
  });
}

function remove(req, res, Entity) {
  var metadata = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var hooks = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  Entity.findByIdAndRemove(req.params.id, function (err, entity) {
    if (err) return res.status(500).send('There was a problem deleting ' + metadata.entityName + ': ' + err.message);

    if (hooks.callback) return hooks.callback(entity);

    return res.json({ message: 'deleted', entity: entity });
  });
}