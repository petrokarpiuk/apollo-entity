'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateEntityActions = generateEntityActions;

var _entity = require('./entity');

var crudEntity = _interopRequireWildcard(_entity);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function generateEntityActions(Entity) {
  var metadata = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var hooks = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var entity = {};

  // if actions only this actions will be created
  if (metadata.only) {
    metadata.only.forEach(function (action) {
      entity[action] = function (req, res) {
        return crudEntity[action](req, res, Entity, metadata, hooks[action]);
      };
    });
  }

  Object.keys(crudEntity).forEach(function (action) {
    entity[action] = function (req, res) {
      return crudEntity[action](req, res, Entity, metadata, hooks[action]);
    };
  });

  return entity;
}