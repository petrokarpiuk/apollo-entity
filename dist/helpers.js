"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateEndpoints = generateEndpoints;
function generateEndpoints(router, controller) {
  var metadata = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  Object.keys(controller).forEach(function (action) {
    if (metadata[action]) {
      router[metadata[action].method](metadata[action].url, controller[action]);
    }
  });
}