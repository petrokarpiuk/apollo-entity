export function getOne(req, res, Entity, metadata = {}, hooks = {}) {
  Entity.findById(req.params.id, (err, entity) => {
    if (err) return res.status(500).send(`There was a problem finding ${metadata.entityName}: ${err.message}`)

    if (!entity) return res.status(404).send(`${metadata.entityName} not found`);

    if (hooks.transform) entities = hooks.transform(entities)
    if (hooks.callback) return hooks.callback(entities);
  
    return res.json(entity)
  })
}

export function getAll(req, res, Entity, metadata = {}, hooks = {}) {
  Entity.find({}, (err, entities) => {
    if (err) return res.status(500).send(`There was a problem: ${err.message}`)

    if (hooks.transform) entities = hooks.transform(entities)
    if (hooks.callback) return hooks.callback(entities);

    return res.json(entities)
  })
}

export function create(req, res, Entity, metadata = {}, hooks = {}) {
  let resource = metadata.fillable ? {} : req.body

  if (metadata.fillable) {
    metadata.fillable.forEach(field => {
      resource[field] = req.body[field]
    })
  }

  if (hooks.transform) resource = hooks.transform(resource)

  const newEntity = new Entity(resource)

  newEntity.save((err, entity) => {
    if (err) return res.status(500).send(`There was a problem creating ${metadata.entityName}: ${err.message}`)
  
    if (hooks.callback) return hooks.callback(entity)

    return res.json({ message: 'created', entity })
  })
}

export function update(req, res, Entity, metadata = {}, hooks = {}) {
  Entity.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, entity) => {
    if (err) return res.status(500).send(`There was a problem updating a ${metadata.entityName}: ${err.message}`)

    if (hooks.callback) return hooks.callback(entity)
    if (hooks.transform) entity = hooks.transform(entity)

    return res.json({ message: 'updated', entity })
  })
}

export function remove(req, res, Entity, metadata = {}, hooks = {}) {
  Entity.findByIdAndRemove(req.params.id, (err, entity) => {
    if (err) return res.status(500).send(`There was a problem deleting ${metadata.entityName}: ${err.message}`);

    if (hooks.callback) return hooks.callback(entity)

    return res.json({ message: 'deleted', entity })
  })
}

