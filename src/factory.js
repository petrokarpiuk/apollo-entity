import * as crudEntity from './entity'

export function generateEntityActions(Entity, metadata = {}, hooks = {}) {
  const entity = {}

  // if actions only this actions will be created
  if (metadata.only) {
    metadata.only.forEach(action => {
      entity[action] = (req, res) => crudEntity[action](req, res, Entity, metadata, hooks[action])
    })
  }

  Object.keys(crudEntity).forEach(action => {
    entity[action] = (req, res) => crudEntity[action](req, res, Entity, metadata, hooks[action])
  })

  return entity
}
