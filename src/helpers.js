export function generateEndpoints(router, controller, metadata = {}) {
  Object.keys(controller).forEach(action => {
    if (metadata[action]) {
      router[metadata[action].method](metadata[action].url, controller[action])
    }
  })
}