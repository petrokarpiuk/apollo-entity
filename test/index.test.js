import { expect } from 'chai';
import { getHello } from '../src/index';

describe('Node library', function() {
	it('should work', function() {
		expect(true).to.be.true;
	})

  it ('should return hello', () => {
    expect(getHello()).to.be.equals('hello');   
  })
})